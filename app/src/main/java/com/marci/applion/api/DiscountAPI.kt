package com.marci.applion.api

import com.marci.applion.data.models.OffersResponse
import com.marci.applion.utils.Constants.Companion.API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface DiscountAPI {


    // https://www.mocky.io/
    // v2/593284a40f0000b0175bfc31
    @GET("v2/593284a40f0000b0175bfc31")
    suspend fun getDiscountOffers(): Response<OffersResponse>
}