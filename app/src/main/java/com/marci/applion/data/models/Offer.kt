package com.marci.applion.data.models

import java.io.Serializable

data class Offer(
    val description: String,
    val endTime: String,
    val id: String,
    val logoUrl: String,
    val partnerName: String,
    val shops: List<String>,
    val state: String,
    val subtitle: String,
    val title: String
) : Serializable