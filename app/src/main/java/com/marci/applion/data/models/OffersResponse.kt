package com.marci.applion.data.models

data class OffersResponse(
    val offerList: MutableList<Offer>
)