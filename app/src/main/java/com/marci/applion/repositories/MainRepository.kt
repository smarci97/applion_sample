package com.marci.applion.repositories

import com.marci.applion.api.RetrofitInstance

class MainRepository {

    suspend fun getDiscountOffers() = RetrofitInstance.api.getDiscountOffers()
}