package com.marci.applion.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.marci.applion.R
import com.marci.applion.data.models.Offer

class OffersAdapter : RecyclerView.Adapter<OffersAdapter.ViewHolder>() {

    var onItemClick : ((Offer) -> Unit)? = null

    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val tvPartnerName : AppCompatTextView = item.findViewById(R.id.tv_partnername)
        val ivLogo : AppCompatImageView = item.findViewById(R.id.iv_logo)
        val tvTitle : AppCompatTextView = item.findViewById(R.id.tv_title)
        val tvEndtime : AppCompatTextView = item.findViewById(R.id.tv_endtime)
    }

    private val differCallback = object : DiffUtil.ItemCallback<Offer>() {
        override fun areItemsTheSame(oldItem: Offer, newItem: Offer): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Offer, newItem: Offer): Boolean {
            return oldItem.title == newItem.title
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = differ.currentList[position]

        holder.itemView.apply {
            Glide
                .with(this)
                .load(currentItem.logoUrl)
                .override(600,600)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivLogo)
            holder.tvTitle.text = currentItem.title
            holder.tvPartnerName.text = currentItem.partnerName
            holder.tvEndtime.text = currentItem.endTime

            setOnClickListener {
                onItemClick?.invoke(currentItem)
            }
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submitList(list : List<Offer>){
        differ.submitList(list)
    }
}