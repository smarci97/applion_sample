package com.marci.applion.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.marci.applion.R
import com.marci.applion.data.models.Offer
import com.marci.applion.databinding.FragmentDetailsBinding

class DetailsFragment : Fragment() {

    private var _fragmentDetailsBinding: FragmentDetailsBinding? = null
    private val fragmentDetailsBinding get() = _fragmentDetailsBinding!!
    val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _fragmentDetailsBinding = FragmentDetailsBinding.inflate(inflater, container, false)
        return fragmentDetailsBinding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _fragmentDetailsBinding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val offer = args.offer
        updateUI(offer, view)

    }

    private fun updateUI(offer: Offer, view: View) {
        fragmentDetailsBinding.tvPartnername.text = offer.partnerName
        fragmentDetailsBinding.tvTitle.text = offer.title
        fragmentDetailsBinding.tvSubtitle.text = offer.subtitle
        fragmentDetailsBinding.tvState.text = offer.state
        fragmentDetailsBinding.tvEndtime.text = offer.endTime
        fragmentDetailsBinding.tvDescription.text = offer.description
        Glide.with(view).load(offer.logoUrl).into(fragmentDetailsBinding.ivLogo)



        initShops(offer)
    }

    private fun initShops(offer: Offer) {
        if (offer.shops != null) {
            fragmentDetailsBinding.lvShops.adapter =
                ArrayAdapter<String>(
                    requireContext(),
                    //android.R.layout.simple_list_item_1,
                    R.layout.item_listview,
                    offer.shops
                )

            fragmentDetailsBinding.lvShops.setOnItemClickListener { adapterView, view, position, id ->
                val address = offer.shops[position]

                val url = "https://www.google.com/maps/search/?api=1&query=$address"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            }
        }
    }


}