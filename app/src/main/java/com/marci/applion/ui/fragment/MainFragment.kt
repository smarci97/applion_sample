package com.marci.applion.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.marci.applion.R
import com.marci.applion.data.models.Offer
import com.marci.applion.databinding.FragmentMainBinding
import com.marci.applion.ui.MainActivity
import com.marci.applion.ui.adapters.OffersAdapter
import com.marci.applion.ui.viewmodels.ViewModel
import com.marci.applion.utils.Resource

class MainFragment : Fragment() {

    private var _fragmentMainBinding: FragmentMainBinding? = null
    private val fragmentMainBinding get() = _fragmentMainBinding!!
    private lateinit var viewModel: ViewModel
    private lateinit var offersAdapter: OffersAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel = (activity as MainActivity).viewModel
        //viewModel.getDiscountOffers()


        _fragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)
        return fragmentMainBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        updateUI()
    }

    override fun onDestroy() {
        super.onDestroy()
        _fragmentMainBinding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


//        viewModel = (activity as MainActivity).viewModel

        setDiscountOffers()
    }

    private fun setDiscountOffers() {
        viewModel.discountOffers.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    response.data?.let { offersResponse ->
                        offersAdapter.differ.submitList(offersResponse.offerList.toList())
                    }
                }
                is Resource.Error -> {
                    response.message?.let { message ->
                        Toast.makeText(
                            requireContext(),
                            "An error occured: $message",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        })
    }

    private fun updateUI() {
        offersAdapter = OffersAdapter()
        fragmentMainBinding.recyclerView.apply {
            adapter = offersAdapter
            layoutManager = LinearLayoutManager(activity)
        }

        offersAdapter.onItemClick = {
            val bundle = Bundle()
            bundle.putSerializable("offer", it)
            findNavController().navigate(R.id.action_mainFragment_to_detailsFragment, bundle)
        }


        fragmentMainBinding.etSearch.addTextChangedListener {
            if (it.toString().isNotEmpty()) {
                searchOfferByName()
            } else {
                setDiscountOffers()
            }
        }
    }


    private fun searchOfferByName() {
        val searchedtext: String = fragmentMainBinding.etSearch.text.toString()
        viewModel.discountOffers.observe(viewLifecycleOwner, Observer { response ->
            var filteredList = emptyList<Offer>()
            response.data?.let { offersResponse ->
                filteredList = offersResponse.offerList.filter {
                    it.title.toLowerCase().contains(searchedtext.toLowerCase()) ||
                            //it.description.contains(searchedtext) ||
                            it.partnerName.toLowerCase().contains(searchedtext.toLowerCase())
                            //it.subtitle.contains(searchedtext) ||
                            //it.state.contains(searchedtext) ||
                            //it.endTime.contains(searchedtext)
                }
            }
            offersAdapter.differ.submitList(filteredList.toList())
        })
    }
}
