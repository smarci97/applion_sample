package com.marci.applion.ui.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import androidx.lifecycle.*
import com.marci.applion.data.models.Offer
import com.marci.applion.data.models.OffersResponse
import com.marci.applion.repositories.MainRepository
import com.marci.applion.utils.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class ViewModel(
    app : Application,
    private val repository: MainRepository
    ) : AndroidViewModel(app) {

    val discountOffers : MutableLiveData<Resource<OffersResponse>> = MutableLiveData() // here will be the response
    var discountOffersResponse : OffersResponse? = null

    init {
        getDiscountOffers()
    }

    fun getDiscountOffers() = viewModelScope.launch {
        Log.d("debug", "Adatok frissítve")
        discountOffers.postValue(Resource.Loading())
        try {
            if(hasInternetConnection()){
                val response = repository.getDiscountOffers()
                discountOffers.postValue(handleDiscountOffersResponse(response))
            } else {
                discountOffers.postValue(Resource.Error("No internet connection"))
            }
        } catch (t : Throwable){
            when(t){
                is IOException -> discountOffers.postValue(Resource.Error("Network failure"))
                else -> discountOffers.postValue(Resource.Error("Conversion Error"))
            }
        }
    }

    private fun handleDiscountOffersResponse(response : Response<OffersResponse>) : Resource<OffersResponse> {
        if(response.isSuccessful){
            response.body()?.let { resultResponse ->
                if (discountOffersResponse == null){
                    discountOffersResponse = resultResponse
                } else {
                    val oldOffers = discountOffersResponse?.offerList
                    val newOffers = resultResponse.offerList
                    oldOffers?.addAll(newOffers)
                }
                return Resource.Success(discountOffersResponse ?: resultResponse)
            }
        }
        return Resource.Error(response.message())
    }


    private fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<Application>().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return true
        } else {
            connectivityManager.activeNetworkInfo?.run {
                return when (type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }
            }
        }
        return false
    }
}