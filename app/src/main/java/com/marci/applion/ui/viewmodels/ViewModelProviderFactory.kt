package com.marci.applion.ui.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.marci.applion.repositories.MainRepository

class ViewModelProviderFactory(
    val app: Application,
    val repository: MainRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return com.marci.applion.ui.viewmodels.ViewModel(app, repository) as T
    }
}